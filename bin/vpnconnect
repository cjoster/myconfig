#!/bin/bash

set -euo pipefail

function die {
	echo "ERROR: ${1}"
	exit 1
}

search_key=""
if [ "${1:+x}" == "x" ]; then
	if [ ! -z "${1}" ]; then
        	search_key="${1}"
	fi
elif [ "${PREFERRED_VPN:+x}" == "x" ]; then
	if [ ! -z "${PREFERRED_VPN}" ]; then
		search_key="${PREFERRED_VPN}"
	fi
fi

auto_connect=0
if [ "${VPN_AUTOCONNECT:+x}" == "x" ]; then
	auto_connect=1
fi

OLD_IFS="${IFS}"
IFS="
"
let i=1
matches=0
match=""
for c in $(nmcli con show | sed -n 's/[[:space:]]\+[0-9a-f]\{8\}-\([0-9a-f]\{4\}-\)\{3\}[0-9a-f]\{12\}.*[[:space:]]\+vpn[[:space:]]\+.*//p' | sort); do
        conns[i]="${c}"
	if [ ! -z "${search_key}" ]; then
		if [ ! -z "$(echo "${c}" | grep -i "${search_key}")" ]; then
			let matches=$matches+1
			match="${c}"
		fi
	fi
        let i=i+1
done
IFS="${OLD_IFS}"

if [ "${matches}" == "1" ]; then
	if [ "${auto_connect}" == "1" ]; then
		ans=y
	else
		read -N 1 -p "Connect to \"${match}\"? (Y/n): " ans
		if [ -z "${ans}" ]; then
			ans="y"
		else
			echo
		fi
	fi
else
	ans="n"
fi

[ "${ans}" == "q" -o "${ans}" == "Q" ] && exit 0

if [ "${ans}" !=  "y" -a "${ans}" != "Y" ]; then
	[ ! -z "${search_key}" -a "${ans}" != "n" -a "${ans}" != "N" ] && echo "Error: \"${search_key}\" did not uniquely match a single connection."
	let j=1
	while [ "${j}" -lt "${i}" ]; do
		echo "${j}. ${conns[${j}]}"
		let j=j+1
	done

	good=0
	while [ "${good}" == "0" ]; do
		read -p "Please select a VPN connection: " c
		case $c in
			q*|Q*)
				exit 0 ;;
			''|*[!0-9]*) echo "Please enter a number." ;;
			*)
				if [ "${conns[${c}]:+x}" == "x" ]; then
					good=1
					VPN="${conns[${c}]}"
				else
					echo "Invalid selection."
				fi
			;;
		esac
	done
else
	VPN="${match}"
fi

if [ "$(nmcli conn show id "${VPN}" | grep -E ^VPN.VPN-STATE: | sed 's/.*:[[:space:]]*//')" == "5 - VPN connected" ]; then
        echo "VPN is connected, so nothing to do."
elif [ "$(nmcli networking connectivity check)" == "full" ]; then
	
	if [ ! -p ~/.oath/fifo ]; then
		mkfifo ~/.oath/fifo || die "Could not make fifo to send password to nmcli."
	fi

        echo "We have internet, connecting to \"${VPN}\""
	if [ ! -r ~/.oath/pin ]; then
		read -sp "Enter your pin: " pin
		echo
	else
		pin="$(cat ~/.oath/pin)"
	fi
        (echo "vpn.secrets.password:${pin}$(oathtool --hotp \
            $(gpg2 -qd ~/.oath/key) -c $([ ! -f ~/.oath/counter ] && echo -n 0 > \
            ~/.oath/counter || echo -n $(($(cat ~/.oath/counter)+1)) > \
	    ~/.oath/counter; cat ~/.oath/counter))" > ~/.oath/fifo; rm -f ~/.oath/fifo)  &
        nmcli con up id "${VPN}" passwd-file ~/.oath/fifo || \
            kill -9 %1 > /dev/null 2>&1
else
        echo "It doesn't look like we have internet. Refusing to connect to VPN."
fi
rm -f ~/.oath/fifo
