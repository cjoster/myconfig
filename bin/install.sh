#!/bin/bash

set -xeuo pipefail

cat bashrc > ~/.bashrc
cat bash_custom > ~/.bash_custom
